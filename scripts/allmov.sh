#!/bin/bash
DIR="$1"
shift 1
find "$DIR" -type f \( -iname '*.mp4' -or -iname '*.mkv' -or -iname '*.mpg' -or -iname '*.wmv' -or -iname '*.avi' -or -iname '*.flv' -or -iname '*.mov' -or -iname '*.m4v' -or -iname '*.divx' \) -not -iname '*.*.mkv' -exec "$*" {} \;
