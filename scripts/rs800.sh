HandBrakeCLI -i "$1" -o "$1.mkv" --loose-anamorphic --width 800 -e x264 -f av_mkv
[ $? -eq 0 ]  || exit 1
rm "$1"