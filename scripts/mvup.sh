#!/bin/bash
source=$1
shift
parent=$(dirname $source)
find "$source" -type f -exec mv $* "{}" "$parent/" \;
