#!/bin/bash
for input in $1; do
    output=$2/$(basename "$input")
    echo "$output"
    HandBrakeCLI -i "$input" -o "$output" --pixel-aspect 1:1
done
