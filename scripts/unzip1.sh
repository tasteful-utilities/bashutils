#!/bin/bash
dir=$(dirname $1)
unzip -j "$1" -d "$dir/$(basename $1 .zip)"
[ $? -eq 0 ]  || exit 1
rm "$1"
