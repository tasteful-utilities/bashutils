#!/bin/bash
find /Volumes/media/xfer/complete -type f \( -iname '*.mp4' -or -iname '*.mkv' -or -iname '*.mpg' -or -iname '*.wmv' -or -iname '*.avi' -or -iname '*.flv' -iname '*.mov' -or -iname '*.m4v' \) -exec mv -n {} /Volumes/media/xfer/ \;
find /Volumes/media/xfer/incomplete -type f \( -iname '*.mp4' -or -iname '*.mkv' -or -iname '*.mpg' -or -iname '*.wmv' -or -iname '*.avi' -or -iname '*.flv' -iname '*.mov' -or -iname '*.m4v' \) -exec mv -n {} /Volumes/media/xfer/ \;
