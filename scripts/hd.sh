#!/bin/bash
for input in $1; do
    output=$2/$(basename "$input").mkv
    echo "$output"
    HandBrakeCLI -i "$input" -o "$output" -e x264 -f av_mkv -q 22 -Y 720 -O
done
